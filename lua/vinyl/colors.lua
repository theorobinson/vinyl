--[[
  ISC License

  Copyright (c) 2021 Theo Robinson
 
  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
  REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
  AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
  INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
  LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
  OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
  PERFORMANCE OF THIS SOFTWARE.
]]--

local colors = {}

local colors = {
   -- Thank you gruvbox!
   bg0 = "#282828",
   bg1 = "#3c3836",
   bg2 = "#504945",
   bg3 = "#665c54",
   bg4 = "#7c6f64",
   fg0 = "#fbf1c7",
   fg1 = "#ebdbb2",
   fg2 = "#d5c4a1",
   fg3 = "#bdae93",
   fg4 = "#a89984",
   gray = "#928374",
   red = "#fb4934",
   orange = "#fe8019",
   yellow = "#f4c332",
   green = "#7b8f24",
   aqua = "#8ec07c",
   blue = "#5397ac",
   purple = "#ac5397",
}

return colors
