--[[
  ISC License
  
  Copyright (c) 2021 Theo Robinson
  
  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
  REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
  AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
  INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
  LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
  OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
  PERFORMANCE OF THIS SOFTWARE.
]]--

local colors = require("vinyl.colors")

local M = {}

M.init = function()
   local highlight = function(group, fg, bg, attr, sp)
      fg = fg and "guifg=" .. fg or "guifg=NONE"
      bg = bg and "guibg=" .. bg or "guibg=NONE"
      attr = attr and "gui=" .. attr or "gui=NONE"
      sp = sp and "guisp=" .. sp or ""
      vim.api.nvim_command(
         "highlight "
            .. group
            .. " "
            .. fg
            .. " "
            .. bg
            .. " "
            .. attr
            .. " "
            .. sp
      )
   end

   -- Normal text
   highlight("Normal", colors.fg1, colors.bg0, nil, nil)

   highlight("CursorLine", nil, colors.bg1, nil, nil)
   highlight("CursorColumn", nil, colors.bg1, nil, nil)

   highlight("TablineFill", colors.bg4, colors.bg1, nil, nil)
   highlight("Tabline", colors.bg4, colors.bg1, nil, nil)
   highlight("TablineSel", colors.green, colors.bg1, nil, nil)

   highlight("MatchParen", nil, colors.bg3, "bold", nil)

   highlight("ColorColumn", nil, colors.bg1, nil, nil)
   highlight("Conceal", colors.blue, nil, nil, nil)
   highlight("CursorLineNr", colors.yellow, colors.bg1, nil, nil)

   highlight("NonText", colors.bg2)
   highlight("SpecialKey", colors.bg2)

   highlight("Visual", nil, colors.bg3, "inverse", nil)
   highlight("VisualNOS", "Visual")

   highlight("Search", colors.yellow, colors.bg0, "inverse", nil)
   highlight("Search", colors.orange, colors.bg0, "inverse", nil)

   highlight("Underlined", colors.blue, nil, "underline", nil)

   highlight("StatusLine", colors.bg2, colors.fg1, "inverse", nil)
   highlight("StatusLineNC", colors.bg1, colors.fg4, "inverse", nil)

   highlight("VertSplit", colors.bg3, colors.bg0, nil, nil)
   highlight("WildMenu", colors.blue, colors.bg3, "bold", nil)
   highlight("Directory", colors.green, nil, "bold", nil)
   highlight("Title", colors.green, nil, "bold", nil)
   highlight("ErrorMsg", colors.bg0, colors.red, "bold", nil)
   highlight("MoreMsg", colors.yellow, nil, "bold", nil)
   highlight("ModeMsg", colors.yellow, nil, "bold", nil)
   highlight("Question", colors.orange, nil, "bold", nil)
   highlight("WarningMsg", colors.yellow, nil, "bold", nil)

   --[[
     Gutter
   --]]

   highlight("LineNr", colors.bg4, nil, nil, nil)
   highlight("SignColumn", nil, colors.bg1, nil, nil)
   highlight("Folded", colors.gray, colors.bg1, "italic", nil)
   highlight("FoldColumn", colors.gray, colors.bg1, nil, nil)

   --[[
     Cursor
   --]]

   highlight("Cursor", nil, nil, "inverse", nil)
   highlight("vCursor", nil, nil, "inverse", nil)
   highlight("iCursor", nil, nil, "inverse", nil)
   highlight("lCursor", nil, nil, "inverse", nil)

   --[[
     Actual syntax highlighting
   --]]

   highlight("Special", colors.orange, nil, nil, nil)

   highlight("Comment", colors.bg3, nil, "italic", nil)
   highlight("Todo", colors.red, nil, "bold," .. "italic", nil)
   highlight("Error", colors.red, nil, "bold," .. "inverse", nil)

   -- Generic statement
   highlight("Statement", colors.red, nil, nil, nil)
   -- if, then, else, endif, switch, etc.
   highlight("Conditional", colors.red, nil, nil, nil)
   -- for, do, while, etc.
   highlight("Repeat", colors.red, nil, nil, nil)
   -- case, default, etc.
   highlight("Label", colors.red, nil, nil, nil)
   -- try, catch, throw, etc.
   highlight("Exception", colors.red, nil, nil, nil)
   -- sizeof, "+", "*", etc.
   highlight("Operator", colors.red, nil, nil, nil)
   -- Any other keyword
   highlight("Keyword", colors.red, nil, nil, nil)

   -- Variable name
   highlight("Identifier", colors.blue, nil, nil, nil)
   -- Function name
   highlight("Function", colors.green, nil, "bold", nil)

   -- Generic preprocessor
   highlight("PreProc", colors.aqua, nil, nil, nil)
   -- Preprocessor #include
   highlight("Include", colors.aqua, nil, nil, nil)
   -- Preprocessor #define
   highlight("Define", colors.aqua, nil, nil, nil)
   -- Same as Define
   highlight("Macro", colors.aqua, nil, nil, nil)
   -- Preprocessor #if, #else, #endif, etc.
   highlight("PreCondit", colors.aqua, nil, nil, nil)

   -- Generic constant
   highlight("Constant", colors.purple, nil, nil, nil)
   -- Character constant: 'c', '/n'
   highlight("Character", colors.purple, nil, "bold", nil)
   -- String constant: "this is a string"
   highlight("String", colors.green, nil, "italic", nil)
   -- Boolean constant: TRUE, false
   highlight("Boolean", colors.purple, nil, nil, nil)
   -- Number constant: 234, 0xff
   highlight("Number", colors.purple)
   -- Floating point constant: 2.3e10
   highlight("Float", colors.purple, nil, nil, nil)

   -- Generic type
   highlight("Type", colors.yellow, nil, nil, nil)
   -- static, register, volatile, etc
   highlight("StorageClass", colors.orange, nil, nil, nil)
   -- struct, union, enum, etc.
   highlight("Structure", colors.yellow, nil, nil, nil)
   -- typedef
   highlight("Typedef", colors.aqua, nil, nil, nil)

   -- Popup menu: normal item
   highlight("Pmenu", colors.fg1, colors.bg2, nil, nil)
   -- Popup menu: selected item
   highlight("PmenuSel", colors.bg2, colors.blue, "bold", nil)
   -- Popup menu: scrollbar
   highlight("PmenuSbar", nil, colors.bg2, nil, nil)
   -- Popup menu: scrollbar thumb
   highlight("PmenuThumb", nil, colors.bg4, nil, nil)

   highlight("DiffAdd", colors.green, colors.bg0, "inverse", nil)
   highlight("DiffDelete", colors.red, colors.bg0, "inverse", nil)
   highlight("DiffChange", colors.aqua, colors.bg0, "inverse", nil)
   highlight("DiffText", colors.yellow, colors.bg0, "inverse", nil)

   -- Uncapitalized words, or compile warnings
   highlight("SpellCap", nil, nil, "undercurl", colors.red)
   -- Unrecognized word
   highlight("SpellBad", nil, nil, "undercurl", colors.blue)
   -- Wrong spelling for selected region
   highlight("SpellLocal", nil, nil, "undercurl", colors.aqua)
   -- Rare word
   highlight("SpellRare", nil, nil, "undercurl", colors.purple)
end

return M
